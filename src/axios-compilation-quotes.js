import axios from 'axios';

const basic = axios.create({
  baseURL: 'https://quotes-sobolev.firebaseio.com/'
});

export default basic;