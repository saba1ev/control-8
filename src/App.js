import React, {Component, Fragment} from 'react';
import {NavLink as RouterNavLink, Route, Switch} from 'react-router-dom';
import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import './App.css';
import AddQuote from "./Container/AddQuote/AddQuote";
import QuoteList from "./Container/QuoteList/QuoteList";
import EditQuote from "./Container/EditQuote/EditQuote";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Navbar color="dark" dark expand="md">
          <NavbarBrand className='Light'>Qoutes Cental</NavbarBrand>
          <NavbarToggler/>
          <Collapse isOpen navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink tag={RouterNavLink} to="/" exact>Qoutes</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={RouterNavLink} to="/add-quote">Submit new quote</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
        <Container>
          <Switch>
            <Route path='/' exact component={QuoteList}/>
            <Route path='/add-quote' component={AddQuote} />
            <Route path='/quotes/:categoryId/edit' component={EditQuote}/>
            <Route path="/quotes/:categoryId" component={QuoteList}/>


          </Switch>
        </Container>
      </Fragment>
    );
  }
}

export default App;
