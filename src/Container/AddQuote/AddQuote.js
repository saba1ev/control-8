import React, {Component, Fragment} from 'react';
import axios from '../../axios-compilation-quotes';
import CompilationForm from "../../components/Quotes/CompilationForm/CompilationForm";

class AddProduct extends Component {

  addQuoters = quoter => {
    axios.post('quotes.json', quoter).then(() => {
      this.props.history.replace('/');
    })
  };


  render() {
    return (
      <Fragment>
        <h1>Add New Product</h1>
        <CompilationForm onSubmit={this.addQuoters}/>
      </Fragment>
    );
  }
}

export default AddProduct;