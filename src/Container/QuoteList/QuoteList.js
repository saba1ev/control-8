import React, {Component} from 'react';
import {
  Button,
  Card,
  CardBody,
  CardText,
  CardTitle,
  Col,
  Nav,
  NavItem,
  NavLink,
  Row
} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import {CATEGORIES} from "../../Catigory";
import axios from '../../axios-compilation-quotes';


class QuoteList extends Component {

  state = {
    quotes: null
  };

  loadData() {
    let url = 'quotes.json';
    const categoryId = this.props.match.params.categoryId;
    if (categoryId) {
      url += `?orderBy="category"&equalTo="${categoryId}"`;
    }


    axios.get(url).then(response => {
      if (response.data !== null){
        const quotes = Object.keys(response.data).map(id => {
          return {...response.data[id], id};
        });
        this.setState({quotes});
      }else{
        this.setState({quotes: []})
      }

    })
  }

  deleteHendler = (id) =>{
    axios.delete(`quotes/${id}.json`).then(() => {
      this.loadData();
      // this.props.history.push('/');
    })
  };

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.categoryId !== prevProps.match.params.categoryId) {
      this.loadData()
    }
  };


  render() {
    let quotes = null;

    if (this.state.quotes) {
      quotes = this.state.quotes.map(quote => (
        <Card key={quote.id}>
          <CardBody>
            <CardTitle>Author: {quote.author}</CardTitle>
            <CardText>Text: <strong>{quote.text}</strong></CardText>
            <RouterNavLink to={'/quotes/' + quote.id + '/edit'}>
              <Button>Edit</Button>
            </RouterNavLink>
            <RouterNavLink to='/'>
              <Button onClick={()=>this.deleteHendler(quote.id)}>Delete</Button>
            </RouterNavLink>

          </CardBody>
        </Card>
      ));
    }
    return (
      <Row>
        <Col sm={3}>
          <h5>Quotes by Category</h5>
          <Nav vertical>
            <NavItem>
              <NavLink tag={RouterNavLink} to="/" exact>All Quotes</NavLink>
            </NavItem>
            {Object.keys(CATEGORIES).map(categoryId => (
              <NavItem key={categoryId}>
                <NavLink
                  tag={RouterNavLink}
                  to={"/quotes/" + categoryId}
                  exact
                >
                  {CATEGORIES[categoryId]}
                </NavLink>
              </NavItem>
            ))}

          </Nav>
        </Col>
        <Col >
            {quotes}
        </Col>
      </Row>
    );
  }
}

export default QuoteList;