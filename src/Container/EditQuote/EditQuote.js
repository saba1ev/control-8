import React, {Component, Fragment} from 'react';
import axios from '../../axios-compilation-quotes';
import CompilationForm from '../../components/Quotes/CompilationForm/CompilationForm'

class EditQuote extends Component {
  state={
    quotes: null
  };

  getQuotesUrl = () =>{
    const id = this.props.match.params.categoryId;
    return 'quotes/' + id + '.json';
  };
  componentDidMount(){
    axios.get(this.getQuotesUrl()).then(response =>{
      this.setState({quotes: response.data})
    })
  };
  editQuoter = quote =>{
    axios.put(this.getQuotesUrl(), quote).then(()=>{
      this.props.history.replace('/');
    })
  };
  render() {
    let form = <CompilationForm
        onSubmit={this.editQuoter}
        quote={this.state.quotes}
    />;
    if (!this.state.quotes){
      form = <div>Load Edit From...</div>
    }
    return (
      <Fragment>
        <h2>Edit</h2>
        {form}
      </Fragment>

    );
  }
}

export default EditQuote;