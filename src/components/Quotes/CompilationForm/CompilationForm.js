import React, {Component} from 'react';
import {CATEGORIES} from '../../../Catigory';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class CompilationForm extends Component {
  state={
    author: '',
    category: Object.keys(CATEGORIES)[0],
    text: '',
  };
  changeValue = event => {
    const {name, value} = event.target;
    this.setState({[name]: value});
  };

  submitHandler = event => {
    event.preventDefault();
    this.props.onSubmit({...this.state})
  };


  render() {
    return (
        <Form className="ProductForm" onSubmit={this.submitHandler}>
          <FormGroup row>
            <Label for="category" sm={2}>Category</Label>
            <Col sm={8}>
              <Input type="select" name="category" id="category"
                     value={this.state.category}
                     onChange={this.changeValue}>
                {Object.keys(CATEGORIES).map(categoryId => (
                  <option key={categoryId} value={categoryId}>{CATEGORIES[categoryId]}</option>
                ))}
              </Input>
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="name" sm={2}>Author</Label>
            <Col sm={8}>
              <Input type="text" name="author" id="name" placeholder="Author"
                     value={this.state.author} onChange={this.changeValue}
              />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="category" sm={2}>Quote text</Label>
            <Col sm={8}>
              <Input type="textarea" name="text" placeholder="Quote text"
                     value={this.state.text} onChange={this.changeValue}
              />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Col sm={{size: 10, offset: 2}}>
              <Button type="submit">Save</Button>
            </Col>
          </FormGroup>
        </Form>

    );
  }
}

export default CompilationForm;